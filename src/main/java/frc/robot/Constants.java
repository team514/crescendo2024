package frc.robot;

import com.revrobotics.CANSparkBase.IdleMode;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.math.util.Units;
import frc.lib.config.SwerveModuleConstants;

public final class Constants {

  public static final class Linkage {
    public static int linkageMotor = 24;
    public static int linkageLimitID = 2;

    public static double linkageMotorSpeed = 0.2;

    public static int maxLinkagePosition = 160 * 42;

    public static int linkageSource = 108 * 42;
    public static int linkageAmp = 132 * 42;
  }

  public static final class Carriage {
    public static int carriageLeftID = 18;
    public static int carriageRightID = 19;
    public static int ioLeftID = 20;
    public static int ioRightID = 21;
    public static int carriageLimitID = 3;
    public static int carriageBannerSensor = 5;
    public static int carriageEncoder_channelA = 0;
    public static int carriageEncoder_channelB = 1;

    public static double carriageMotorSpeed = 0.25;
    public static double ioLeftSpeed = 1;
    public static double ioRightSpeed = 0.75;

    public static int maxCarriagePosition = 150;

    public static int carriageSource = 0;
    public static int carriageAmp = 147;
  }

  public static final class Climb {
    public static int ctrePCMCanID = 15;
    public static int climbSolenoidID = 0;
    public static int leftClimbID = 22;
    public static int rightClimbID = 23;

    public static double motorSpeed = 1.0;
    public static double motorStopSpeed = 0;
    public static int climbLimitID = 4;
  }

  public static final class Swerve {
    public static final double stickDeadband = 0.1;

    public static final int pigeonID = 13;
    public static final boolean invertGyro = false; // Always ensure Gyro is CCW+ CW-

    /* Drivetrain Constants */
    public static final double trackWidth = Units.inchesToMeters(24.1875);
    public static final double wheelBase = Units.inchesToMeters(24.1875);
    public static final double wheelDiameter = Units.inchesToMeters(4.0);
    public static final double wheelCircumference = wheelDiameter * Math.PI;

    public static final double openLoopRamp = 0.25;
    public static final double closedLoopRamp = 0.0;

    public static final double driveGearRatio = (8.14 / 1.0); // 8.14:1
    public static final double angleGearRatio = ((150 / 7) / 1.0); // ~21:1

    public static final SwerveDriveKinematics swerveKinematics = new SwerveDriveKinematics(
        new Translation2d(wheelBase / 2.0, trackWidth / 2.0), // front left
        new Translation2d(wheelBase / 2.0, -trackWidth / 2.0), // front right
        new Translation2d(-wheelBase / 2.0, trackWidth / 2.0), // back left
        new Translation2d(-wheelBase / 2.0, -trackWidth / 2.0) // back right
    );

    /* Swerve Voltage Compensation */
    public static final double voltageComp = 12.0;

    /* Swerve Current Limiting */
    public static final int angleContinuousCurrentLimit = 20;
    public static final int driveContinuousCurrentLimit = 80;

    /* Angle Motor PID Values */
    public static final double angleKP = 0.01; // P = proportional
    public static final double angleKI = 0.0; // I = integral
    public static final double angleKD = 0.02; // D = derivative
    public static final double angleKFF = 0.0; // FF = feed forward

    /* Drive Motor PID Values */
    public static final double driveKP = 0.1; // P = proportional
    public static final double driveKI = 0.0; // I = integral
    public static final double driveKD = 0.0; // D = derivative
    public static final double driveKFF = 0.0; // FF = feed forward

    /* Drive Motor Characterization Values */
    public static final double driveKS = 0.667; // S = static gain
    public static final double driveKV = 2.44; // V = velocity gain
    public static final double driveKA = 0.27; // A = acceleration gain

    /* Drive Motor Conversion Factors */
    // This represents how many meters we move forward for every drive motor
    // rotation
    public static final double driveConversionPositionFactor = (wheelDiameter * Math.PI) / driveGearRatio;

    // This represents how far in meters the robot would drive in 1 second
    // if the drive motors moved at 1RPM (very slow)
    //
    // The unit for this constant is meters per second per RPM
    public static final double driveConversionVelocityFactor = driveConversionPositionFactor / 60.0;

    // This represents how many degrees the wheel rotates for every angle motor
    // rotation
    public static final double angleConversionFactor = 360.0 / angleGearRatio;

    /* Swerve Profiling Values */
    public static final double maxSpeed = 4.5; // meters per second
    public static final double maxAngularVelocity = 11.5; // radians per second

    /* Neutral Modes */
    public static final IdleMode angleNeutralMode = IdleMode.kCoast;
    public static final IdleMode driveNeutralMode = IdleMode.kBrake;

    /* Motor Inverts */
    public static final boolean driveInvert = false;
    public static final boolean angleInvert = true;

    /* Angle Encoder Invert */
    public static final boolean canCoderInvert = false;

    /* Module Specific Constants */
    /* Front Left Module - Module 0 */
    public static final class Mod0 {
      public static final int angleMotorID = 2;
      public static final int driveMotorID = 1;
      public static final int canCoderID = 9;
      public static final Rotation2d angleOffset = Rotation2d.fromDegrees(76.201);
      public static final SwerveModuleConstants constants = new SwerveModuleConstants(driveMotorID, angleMotorID,
          canCoderID, angleOffset);
    }

    /* Front Right Module - Module 1 */
    public static final class Mod1 {
      public static final int angleMotorID = 8;
      public static final int driveMotorID = 7;
      public static final int canCoderID = 12;
      public static final Rotation2d angleOffset = Rotation2d.fromDegrees(105.117);
      public static final SwerveModuleConstants constants = new SwerveModuleConstants(driveMotorID, angleMotorID,
          canCoderID, angleOffset);
    }

    /* Back Left Module - Module 2 */
    public static final class Mod2 {
      public static final int angleMotorID = 3;
      public static final int driveMotorID = 4;
      public static final int canCoderID = 10;
      public static final Rotation2d angleOffset = Rotation2d.fromDegrees(298.564);
      public static final SwerveModuleConstants constants = new SwerveModuleConstants(driveMotorID, angleMotorID,
          canCoderID, angleOffset);
    }

    /* Back Right Module - Module 3 */
    public static final class Mod3 {
      public static final int angleMotorID = 6;
      public static final int driveMotorID = 5;
      public static final int canCoderID = 11;
      public static final Rotation2d angleOffset = Rotation2d.fromDegrees(56.074);
      public static final SwerveModuleConstants constants = new SwerveModuleConstants(driveMotorID, angleMotorID,
          canCoderID, angleOffset);
    }
  }

  public static final class AutoConstants {
    public static final double kMaxSpeedMetersPerSecond = 3;
    public static final double kMaxAccelerationMetersPerSecondSquared = 3;
    public static final double kMaxAngularSpeedRadiansPerSecond = Math.PI;
    public static final double kMaxAngularSpeedRadiansPerSecondSquared = Math.PI;

    public static final double kPXController = 1;
    public static final double kPYController = 1;
    public static final double kPThetaController = 1;

    // Constraint for the motion profilied robot angle controller
    public static final TrapezoidProfile.Constraints kThetaControllerConstraints = new TrapezoidProfile.Constraints(
        kMaxAngularSpeedRadiansPerSecond, kMaxAngularSpeedRadiansPerSecondSquared);
  }
}


//public static rizz