// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import static edu.wpi.first.units.MutableMeasure.mutable;
import static edu.wpi.first.units.Units.Meters;
import static edu.wpi.first.units.Units.MetersPerSecond;
import static edu.wpi.first.units.Units.Volts;

import com.ctre.phoenix6.configs.Pigeon2Configuration;
import com.ctre.phoenix6.hardware.Pigeon2;

import edu.wpi.first.math.controller.HolonomicDriveController;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveDriveOdometry;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.units.Distance;
import edu.wpi.first.units.Measure;
import edu.wpi.first.units.MutableMeasure;
import edu.wpi.first.units.Velocity;
import edu.wpi.first.units.Voltage;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj2.command.sysid.SysIdRoutine;
import frc.handlers.SwerveModule;
import frc.robot.Constants;
import frc.robot.Robot;
import frc.robot.RobotContainer;

public class DriveUtil extends SubsystemBase {
  /* Unchanging objects */
  private final Pigeon2 gyro;
  private final SwerveDriveOdometry swerveOdometry;
  private final SwerveModule[] swerveModules;
  private final Field2d field;
  private final ChassisSpeeds lastSpeeds = new ChassisSpeeds();

  /* Changing objects */
  private boolean fieldRelative = true;
  private final PIDController fieldXPIDController = new PIDController(0.1, 0, 0);
  private final PIDController fieldYPIDController = new PIDController(0.1, 0, 0);
  private final ProfiledPIDController fieldRotationPIDController = new ProfiledPIDController(1, 0, 0, new TrapezoidProfile.Constraints(6.28, 3.14));
  private final HolonomicDriveController holonomicDriveController = new HolonomicDriveController(fieldXPIDController, fieldYPIDController, fieldRotationPIDController);

  private SysIdRoutine m_sysIdRoutine;

    // Mutable holder for unit-safe voltage values, persisted to avoid reallocation.
  private final MutableMeasure<Voltage> m_appliedVoltage = mutable(Volts.of(0));
  // Mutable holder for unit-safe linear distance values, persisted to avoid reallocation.
  private final MutableMeasure<Distance> m_distance = mutable(Meters.of(0));
  // Mutable holder for unit-safe linear velocity values, persisted to avoid reallocation.
  private final MutableMeasure<Velocity<Distance>> m_velocity = mutable(MetersPerSecond.of(0));

  /** Creates a new DriveUtil. */
  public DriveUtil() {
    gyro = new Pigeon2(Constants.Swerve.pigeonID);
    gyro.getConfigurator().apply(new Pigeon2Configuration());
    zeroGyro();

    swerveOdometry = new SwerveDriveOdometry(Constants.Swerve.swerveKinematics, new Rotation2d(),
      new SwerveModulePosition[] {
        new SwerveModulePosition(),
        new SwerveModulePosition(),
        new SwerveModulePosition(),
        new SwerveModulePosition()
      }
    );

    swerveModules =
        new SwerveModule[] {
          new SwerveModule(0, Constants.Swerve.Mod0.constants),
          new SwerveModule(1, Constants.Swerve.Mod1.constants),
          new SwerveModule(2, Constants.Swerve.Mod2.constants),
          new SwerveModule(3, Constants.Swerve.Mod3.constants)
        };

    field = new Field2d();
    SmartDashboard.putData("Field", field);

    m_sysIdRoutine = new SysIdRoutine(
          // Empty config defaults to 1 volt/second ramp rate and 7 volt step voltage.
          new SysIdRoutine.Config(),
          new SysIdRoutine.Mechanism(
              // Tell SysId how to plumb the driving voltage to the motors.
              (Measure<Voltage> volts) -> {
                swerveModules[0].getDriveMotor().setVoltage(volts.in(Volts));
                swerveModules[1].getDriveMotor().setVoltage(volts.in(Volts));
                swerveModules[2].getDriveMotor().setVoltage(volts.in(Volts));
                swerveModules[3].getDriveMotor().setVoltage(volts.in(Volts));
              },
              // Tell SysId how to record a frame of data for each motor on the mechanism being
              // characterized.
              log -> {
                // Record a frame for the left motors.  Since these share an encoder, we consider
                // the entire group to be one motor.
                log.motor("drive-left")
                    .voltage(
                        m_appliedVoltage.mut_replace(
                            swerveModules[0].getDriveMotor().get() * RobotController.getBatteryVoltage(), Volts))
                    .linearPosition(m_distance.mut_replace(swerveModules[0].getDriveMotor().getEncoder().getPosition(), Meters))
                    .linearVelocity(
                        m_velocity.mut_replace(swerveModules[0].getDriveMotor().getEncoder().getVelocity(), MetersPerSecond));
                // Record a frame for the right motors.  Since these share an encoder, we consider
                // the entire group to be one motor.
                log.motor("drive-right")
                    .voltage(
                        m_appliedVoltage.mut_replace(
                            swerveModules[1].getDriveMotor().get() * RobotController.getBatteryVoltage(), Volts))
                    .linearPosition(m_distance.mut_replace(swerveModules[1].getDriveMotor().getEncoder().getPosition(), Meters))
                    .linearVelocity(
                        m_velocity.mut_replace(swerveModules[1].getDriveMotor().getEncoder().getVelocity(), MetersPerSecond));
              },
              // Tell SysId to make generated commands require this subsystem, suffix test state in
              // WPILog with this subsystem's name ("drive")
              this));
  }

  public Command sysIdQuasistatic(SysIdRoutine.Direction direction) {
    return m_sysIdRoutine.quasistatic(direction);
  }

  public Command sysIdDynamic(SysIdRoutine.Direction direction) {
    return m_sysIdRoutine.dynamic(direction);
  }


  public Rotation2d getYaw() {
    return (Constants.Swerve.invertGyro)
        ? Rotation2d.fromDegrees(360 - gyro.getYaw().getValueAsDouble())
        : Rotation2d.fromDegrees(gyro.getYaw().getValueAsDouble());
  }

  public Pose2d getPose() {
    return swerveOdometry.getPoseMeters();
  }

  public SwerveModulePosition[] getPositions() {
    SwerveModulePosition[] positions = new SwerveModulePosition[4];
    for (SwerveModule mod : swerveModules) {
      positions[mod.moduleNumber] = mod.getPosition();
    }
    return positions;
  }

  public SwerveModuleState[] getStates() {
    SwerveModuleState[] states = new SwerveModuleState[4];
    for (SwerveModule mod : swerveModules) {
      states[mod.moduleNumber] = mod.getState();
    }
    return states;
  }

  public boolean isFieldRelative() {
    return fieldRelative;
  }

  public void resetOdometry(Pose2d pose) {
    swerveOdometry.resetPosition(getYaw(), getPositions(), pose);
  }

  public void wheelsToHome() {
    for (SwerveModule mod : swerveModules) {
      mod.moveWheelToHome();
    }
  }

  public void zeroGyro() {
    gyro.setYaw(0);
  }

  public void toggleRobotCentric() {
    fieldRelative = !fieldRelative;
    SmartDashboard.putBoolean("Robot Centric", fieldRelative);
  }

  /**
   * Drive the robot! This is the only method that manages the movement of the robot's swerve drive train.
   * This method should be used for joystick driving *and* automated driving.
   * 
   * @param translation   The X/Y components of the desired robot movement, in meters per second
   * @param rotation      The rotational component of the desired robot movement, in radians per second
   * @param fieldRelative True if movement should be relative to the field, false if relative to the robot
   * @param isClosedLoop  True if PID should manage movement of the drive motors, false if otherwise; PID always manages angle motors
   */
  public void driveRobot(Translation2d translation, double rotation, boolean fieldRelative, boolean isClosedLoop) {
    // If we want to drive relative to the field, we need to perform this calculation
    // This comes from ChassisSpeeds.fromFieldRelativeSpeeds()
    // if (fieldRelative) translation.rotateBy(getYaw().unaryMinus());
     if (fieldRelative) {
      ChassisSpeeds localLastSpeeds = ChassisSpeeds.fromFieldRelativeSpeeds(translation.getX(), translation.getY(), rotation, getYaw());
      lastSpeeds.vxMetersPerSecond = localLastSpeeds.vxMetersPerSecond;
      lastSpeeds.vyMetersPerSecond = localLastSpeeds.vyMetersPerSecond;
      lastSpeeds.omegaRadiansPerSecond = localLastSpeeds.omegaRadiansPerSecond;
     } else {
    // Update the lastSpeeds ChassisSpeeds object with the most up-to-date movement speeds
      lastSpeeds.vxMetersPerSecond = translation.getX();
      lastSpeeds.vyMetersPerSecond = translation.getY();
      lastSpeeds.omegaRadiansPerSecond = rotation;
     }

    driveChassisSpeeds(lastSpeeds, isClosedLoop);
  }

  public void driveChassisSpeeds(ChassisSpeeds chassisSpeeds, boolean isClosedLoop){
    // Calculate SwerveModuleState for each module based on input translation and rotation values
    // These states represent the target angles and target velocities for each wheel
    SwerveModuleState[] swerveModuleStates = Constants.Swerve.swerveKinematics.toSwerveModuleStates(chassisSpeeds);

    // Set the desired state for each module based on the above calculations
    setModuleStates(swerveModuleStates, isClosedLoop);
  }

  /**
   * Set the desired state for each module
   * 
   * @param desiredStates an array of SwerveModuleStates, must contain exactly four objects in the array
   * @param isClosedLoop True if PID should manage movement of the drive motors, false if otherwise; PID always manages angle motors
   */
  public void setModuleStates(SwerveModuleState[] desiredStates, boolean isClosedLoop) {
    // Renormalizes the wheel speeds if any individual speed is above the specified maximum
    SwerveDriveKinematics.desaturateWheelSpeeds(desiredStates, Constants.Swerve.maxSpeed);

    // Set the desired state for each module based on the above calculations
    for (SwerveModule mod : swerveModules) {
      mod.setDesiredState(desiredStates[mod.moduleNumber], isClosedLoop);
    }
  }

  public void driveTrajectory(Trajectory.State goal) {
    ChassisSpeeds adjustSpeeds = holonomicDriveController.calculate(getPose(), goal, Rotation2d.fromDegrees(70));
    // adjustSpeeds = adjustSpeeds.div(50);
    driveChassisSpeeds(adjustSpeeds, false);
  }

  private long lastEncoderRealignment = System.currentTimeMillis();
  private long lastVisionRealignment = System.currentTimeMillis();

  @Override
  public void periodic() {
    if (System.currentTimeMillis() - lastEncoderRealignment >= 2000) {
      lastEncoderRealignment = System.currentTimeMillis();
      for (SwerveModule mod : swerveModules) {
        mod.resetToAbsolute();
      }
    }

    if (!Robot.isEnabled) {
      resetOdometry(RobotContainer.visionUtil.getRobotPosition());
    }

    // if (System.currentTimeMillis() - lastVisionRealignment >= 500) {
    //   lastVisionRealignment = System.currentTimeMillis();
    //   resetOdometry(RobotContainer.visionUtil.getRobotPosition());
    // }

    swerveOdometry.update(getYaw(), getPositions());
    field.setRobotPose(getPose());


  }
}
