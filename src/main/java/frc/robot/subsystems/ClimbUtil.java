// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ClimbUtil extends SubsystemBase {

  WPI_VictorSPX left, right;
  Solenoid solenoid;

  DigitalInput climbLimit;

  boolean bSolenoidUp = false;

  /** Creates a new ClimbUtil. */
  public ClimbUtil() {
    left = new WPI_VictorSPX(Constants.Climb.leftClimbID);
    right = new WPI_VictorSPX(Constants.Climb.rightClimbID);
    left.setNeutralMode(NeutralMode.Coast);
    right.setNeutralMode(NeutralMode.Coast);
    right.follow(left);
    right.setInverted(true);

    solenoid = new Solenoid(Constants.Climb.ctrePCMCanID, PneumaticsModuleType.CTREPCM, Constants.Climb.climbSolenoidID);
    
    climbLimit = new DigitalInput(Constants.Climb.climbLimitID);

  }

  public boolean isLimitPressed() {
    return !climbLimit.get();
  }

  public void grappleState(boolean isUp) {
    bSolenoidUp = isUp;
    solenoid.set(bSolenoidUp);
  }

  public boolean getGrappleState() {
    return solenoid.get();
  }

  public void toggleGrapple() {
    bSolenoidUp = !bSolenoidUp;
    solenoid.set(bSolenoidUp);
  }

  public void setMotorSpeed(double speed) {
    left.set(speed);
  }

  @Override
  public void periodic() {
  }
}
