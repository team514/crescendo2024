// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.util.Optional;

import org.photonvision.EstimatedRobotPose;
import org.photonvision.PhotonCamera;
import org.photonvision.PhotonPoseEstimator;
import org.photonvision.PhotonPoseEstimator.PoseStrategy;

import edu.wpi.first.apriltag.AprilTagFieldLayout;
import edu.wpi.first.apriltag.AprilTagFields;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.net.PortForwarder;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotContainer;

public class VisionUtil extends SubsystemBase {
  public static final AprilTagFieldLayout aprilTagFieldLayout = AprilTagFields.k2024Crescendo.loadAprilTagLayoutField();
  // X = 3.75in / 0.095m
  // Y = 13.25in / 0.34m
  // Z = 28.5in / 0.72m
  public static final Transform3d robotToCam = new Transform3d(new Translation3d(0.095, 0.34, 0.72), new Rotation3d(0,22, Math.PI)); // Measurements in meters
  
  private PhotonPoseEstimator photonPoseEstimator;
  private PhotonCamera camera;
  
  // private final Field2d field;
  private Pose2d lastPosition = new Pose2d();

  /** Creates a new VisionUtil. */
  public VisionUtil() {
    camera = new PhotonCamera(NetworkTableInstance.getDefault(), "Arducam_OV9281_USB_Camera");

    // Construct PhotonPoseEstimator
    photonPoseEstimator = new PhotonPoseEstimator(aprilTagFieldLayout, PoseStrategy.LOWEST_AMBIGUITY, camera, robotToCam);

    // PortForwarder.add(5800, "10.5.14.209", 5800);

    // field = new Field2d();
    // SmartDashboard.putData("Field", field);
  }

  public Optional<EstimatedRobotPose> getEstimatedGlobalPose(Pose2d prevEstimatedRobotPose) {
    photonPoseEstimator.setReferencePose(prevEstimatedRobotPose);
    return photonPoseEstimator.update();
    // return Optional.empty();
  }

  public Pose2d getRobotPosition() {
    return lastPosition;
  }

  public boolean hasTarget() {
    return getEstimatedGlobalPose(lastPosition).isPresent();
  }

  @Override
  public void periodic() {
    Optional<EstimatedRobotPose> opt = getEstimatedGlobalPose(lastPosition);
    if (opt.isPresent()) {
      lastPosition = opt.get().estimatedPose.toPose2d();
    }
  }
}
