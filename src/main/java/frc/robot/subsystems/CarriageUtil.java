// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class CarriageUtil extends SubsystemBase {

  WPI_TalonSRX carriageLeft, carriageRight;
  WPI_VictorSPX ioMotorLeft, ioMotorRight;

  DigitalInput carriageLimit, carriageBannerSensor;

  PIDController carriagePidController = new PIDController(0.08, 0, 0);

  Encoder carriageEncoder;

  /** Creates a new ArmUtil. */
  public CarriageUtil() {
    carriageLeft = new WPI_TalonSRX(Constants.Carriage.carriageLeftID);
    carriageRight = new WPI_TalonSRX(Constants.Carriage.carriageRightID);
    carriageLeft.setNeutralMode(NeutralMode.Brake);
    carriageRight.setNeutralMode(NeutralMode.Brake);
    carriageRight.follow(carriageLeft);
    carriageRight.setInverted(true);

    ioMotorLeft = new WPI_VictorSPX(Constants.Carriage.ioLeftID);
    ioMotorRight = new WPI_VictorSPX(Constants.Carriage.ioRightID);
    ioMotorLeft.setNeutralMode(NeutralMode.Brake);
    ioMotorRight.setNeutralMode(NeutralMode.Brake);
    // ioMotorRight.follow(ioMotorLeft);
    ioMotorLeft.setInverted(false);
    ioMotorRight.setInverted(false);

    carriageLimit = new DigitalInput(Constants.Carriage.carriageLimitID);
    carriageBannerSensor = new DigitalInput(Constants.Carriage.carriageBannerSensor);

    carriageEncoder = new Encoder(Constants.Carriage.carriageEncoder_channelA,
        Constants.Carriage.carriageEncoder_channelB);
  }

  public int getCarriagePositionTicks() {
    return carriageEncoder.get() * -1;
  }

  public void setCarriageSpeed(double speed) {
    carriageLeft.set(speed);
  }

  public void setCarriageSetpoint(double setpoint) {
    if (setpoint >= Constants.Carriage.maxCarriagePosition) {
      return;
    }
    carriagePidController.setSetpoint(setpoint);
    SmartDashboard.putNumber("Carriage Setpoint", setpoint);
  }

  public void calculatePIDController() {
    double voltage = carriagePidController.calculate(getCarriagePositionTicks());
    SmartDashboard.putNumber("Carriage Voltage", voltage);
    carriageLeft.setVoltage(voltage * -1);
  }

  public boolean isAtSetpoint() {
    return carriagePidController.atSetpoint();
  }

  public void setIOSpeed(double leftSpeed, double rightSpeed) {
    ioMotorLeft.set(leftSpeed);
    ioMotorRight.set(rightSpeed);
  }

  public boolean isLimitPressed() {
    return carriageLimit.get();
  }

  public boolean isNoteInCarriage() {
    return carriageBannerSensor.get();
  }

  public void resetEncoder() {
    carriageEncoder.reset();
  }

  

  @Override
  public void periodic() {
    SmartDashboard.putBoolean("Carriage Limit Switch", carriageLimit.get());
    SmartDashboard.putNumber("Carriage Ticks", getCarriagePositionTicks());
    SmartDashboard.putBoolean("Carriage Banner Sensor", isNoteInCarriage());

  }
}
