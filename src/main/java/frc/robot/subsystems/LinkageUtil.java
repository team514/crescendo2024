// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class LinkageUtil extends SubsystemBase {

  CANSparkMax linkageMotor;
  DigitalInput linkageLimit;

  PIDController linkagePidController;

  RelativeEncoder linkageEncoder;

  /** Creates a new ArmUtil. */
  public LinkageUtil() {
    linkageMotor = new CANSparkMax(Constants.Linkage.linkageMotor, MotorType.kBrushless);
    linkageLimit = new DigitalInput(Constants.Linkage.linkageLimitID);

    linkagePidController = new PIDController(0.01, 0, 0);
    linkagePidController.setTolerance(50);

    linkageEncoder = linkageMotor.getEncoder();
  }

  public void setLinkageSetpoint(double encoderTicksSetpoint) {
    if (encoderTicksSetpoint >= Constants.Linkage.maxLinkagePosition) {
      // You can't move further than the max position!
      return;
    }
    linkagePidController.setSetpoint(encoderTicksSetpoint);
  }

  public void calculatePIDController() {
    double voltage = linkagePidController.calculate(getLinkagePositionTicks());
    linkageMotor.setVoltage(voltage);
    SmartDashboard.putNumber("Linkage PID setpoint", linkagePidController.getSetpoint());
    SmartDashboard.putNumber("Linkage PID voltage", voltage);
  }

  public double getLinkagePositionTicks() {
    return linkageEncoder.getPosition() * 42;
  }

  public double getLinkageSetpoint() {
    return linkagePidController.getSetpoint();
  }

  public boolean isAtSetpoint() {
    return linkagePidController.atSetpoint();
  }

  public boolean isLimitPressed() {
    return linkageLimit.get();
  }

  public void resetEncoder() {
    linkageEncoder.setPosition(0);
  }

  public void overrideMotorSpeed(double speed) {
    linkageMotor.set(speed);
  }
  
  @Override
  public void periodic() {
      SmartDashboard.putBoolean("Linkage Limit Switch", linkageLimit.get());
      SmartDashboard.putNumber("Linkage Ticks", getLinkagePositionTicks());
      SmartDashboard.putNumber("Linkage Error", linkagePidController.getPositionError());
  }
}