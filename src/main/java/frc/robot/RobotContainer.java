// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.io.File;
import java.io.IOException;

import edu.wpi.first.math.trajectory.TrajectoryUtil;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PS4Controller;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.button.CommandPS4Controller;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.IOIntake;
import frc.robot.commands.OperateCarriage;
import frc.robot.commands.OperateCarriageManual;
import frc.robot.commands.OperateClimb;
import frc.robot.commands.OperateIO;
import frc.robot.commands.OperateLinkage;
import frc.robot.commands.OperateLinkageManual;
import frc.robot.commands.TeleopSwerve;
import frc.robot.commands.auto.AimToShootInAmp;
import frc.robot.commands.auto.AutoDrive;
import frc.robot.commands.auto.AutoDumpAmp;
import frc.robot.commands.auto.DriveToAmp;
import frc.robot.commands.auto.HomeCarriageAndLinkage;
import frc.robot.commands.auto.MoveLinkageAndCarriage;
import frc.robot.commands.auto.ShootInAmp;
import frc.robot.commands.auto.TrajectoryDrive;
import frc.robot.commands.auto.UpdatePositionFromVision;
import frc.robot.commands.auto.ZeroLinkageAndCradle;
import frc.robot.subsystems.CarriageUtil;
import frc.robot.subsystems.ClimbUtil;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.LinkageUtil;
import frc.robot.subsystems.VisionUtil;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in
 * the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of
 * the robot (including
 * subsystems, commands, and trigger mappings) should be declared here.
 */
public class RobotContainer {
  /* Controllers */

  private static final CommandPS4Controller operator = new CommandPS4Controller(1); // logitech controller
  private static final CommandXboxController driver = new CommandXboxController(0); // xbox controller

  /* Drive Controls */
  private final int translationAxis = Joystick.AxisType.kY.value; // forward-backward movement
  private final int strafeAxis = Joystick.AxisType.kX.value; // left-right movement (strafing)
  private final int rotationAxis = Joystick.AxisType.kZ.value; // rotational movement

  /* Driver Buttons */
  private JoystickButton wheelsToHome,
      zeroGyro, robotCentric, testTrajectory,
      deployClimb, climb,
      runIntake, runOuttake,
      moveToHome, moveToSource, moveToAmp, moveToClimb,
      homeSystem, slowIsSmooth;

  /* Operator Buttons */
  // private JoystickButton ;

  /* Autonomous Commands */
  private Command autoDriveThreeFeet;
  private SendableChooser<Command> autoSendableChooser = new SendableChooser<>();

  /* Subsystems */
  private final DriveUtil driveUtil = new DriveUtil();
  private final LinkageUtil linkageUtil = new LinkageUtil();
  private final ClimbUtil climbUtil = new ClimbUtil();
  private final CarriageUtil carriageUtil = new CarriageUtil();
  public static final VisionUtil visionUtil = new VisionUtil();

  private static boolean isSlow = false;

  /**
   * The container for the robot. Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    // Configure the trigger bindings
    configureBindings();
    // Configure the subsystem default commands
    configureDefaultCommands();
  }

  /**
   * Method used to configure Joystick Buttons and bind them to physical buttons
   */
  private void configureBindings() {
    // Driver Buttons
    robotCentric = new JoystickButton(driver.getHID(), Button.kA.value);
    deployClimb = new JoystickButton(driver.getHID(), Button.kBack.value);
    climb = new JoystickButton(driver.getHID(), Button.kStart.value);
    slowIsSmooth = new JoystickButton(driver.getHID(), Button.kRightBumper.value);

    robotCentric.onTrue(new InstantCommand(() -> driveUtil.toggleRobotCentric(), driveUtil));
    deployClimb.onTrue(new InstantCommand(() -> climbUtil.toggleGrapple()));
    climb.whileTrue(new OperateClimb(climbUtil, Constants.Climb.motorSpeed));

    slowIsSmooth.whileTrue(new RunCommand(() -> {
      isSlow = true;
    }, visionUtil));

    slowIsSmooth.whileFalse(new RunCommand(() -> {
      isSlow = false;
    }, visionUtil));

    // Operator Buttons
    moveToHome = new JoystickButton(operator.getHID(), Button.kA.value);
    moveToSource = new JoystickButton(operator.getHID(), Button.kB.value);
    moveToAmp = new JoystickButton(operator.getHID(), Button.kX.value);
    moveToClimb = new JoystickButton(operator.getHID(), Button.kY.value);

    moveToHome.whileTrue(new MoveLinkageAndCarriage(linkageUtil, carriageUtil, 0, 0));
    moveToSource.whileTrue(new MoveLinkageAndCarriage(linkageUtil, carriageUtil, Constants.Carriage.carriageSource,
        Constants.Linkage.linkageSource));
    moveToAmp.whileTrue(new MoveLinkageAndCarriage(linkageUtil, carriageUtil, Constants.Carriage.carriageAmp,
        Constants.Linkage.linkageAmp));
    // moveToClimb.whileTrue(new MoveLinkageAndCarriage(linkageUtil, carriageUtil,
    // 30, 0));

    runIntake = new JoystickButton(operator.getHID(), Button.kRightBumper.value);
    runOuttake = new JoystickButton(operator.getHID(), Button.kLeftBumper.value);

    runIntake.whileTrue(new IOIntake(carriageUtil));
    runOuttake.whileTrue(new OperateIO(carriageUtil, -1, -0.75));

    homeSystem = new JoystickButton(operator.getHID(), Button.kBack.value);
    // homeSystem.whileTrue(new HomeCarriageAndLinkage(carriageUtil, linkageUtil));

    // Rizz Buttons
    // Autonomous Commands
    autoDriveThreeFeet = new AutoDrive(3, driveUtil);

    autoSendableChooser.setDefaultOption("Zero", new HomeCarriageAndLinkage(carriageUtil, linkageUtil));

    // TrajectoryUtil.fromPathweaverJson(
    // Filesystem.getDeployDirectory().toPath().resolve("paths/First.wpilib.json")
    // );

    // create sendable choosers for auto modes

    // Attempt to create auto sendable choosers from trajectory files.
    // If any file is missing, exception will be thrown, and the following sendable
    // choosers will not be created

    // "Trajectory Directory"
    try {
      autoSendableChooser.addOption("Blue Line Left", new SequentialCommandGroup(
          new UpdatePositionFromVision(visionUtil, driveUtil),
          new HomeCarriageAndLinkage(carriageUtil, linkageUtil),
          new TrajectoryDrive(driveUtil, visionUtil, getTrajectoryFromPath("Blue Line Left"))));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      autoSendableChooser.addOption("Blue Line Middle", new SequentialCommandGroup(
          new UpdatePositionFromVision(visionUtil, driveUtil),
          new HomeCarriageAndLinkage(carriageUtil, linkageUtil),
          new TrajectoryDrive(driveUtil, visionUtil, getTrajectoryFromPath("Blue Line Middle"))));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      autoSendableChooser.addOption("Blue Line Right", new SequentialCommandGroup(
          new UpdatePositionFromVision(visionUtil, driveUtil),
          new HomeCarriageAndLinkage(carriageUtil, linkageUtil),
          new TrajectoryDrive(driveUtil, visionUtil, getTrajectoryFromPath("Blue Line Right"))));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      autoSendableChooser.addOption("Red Line Left", new SequentialCommandGroup(
          new UpdatePositionFromVision(visionUtil, driveUtil),
          new HomeCarriageAndLinkage(carriageUtil, linkageUtil),
          new TrajectoryDrive(driveUtil, visionUtil, getTrajectoryFromPath("Red Line Left"))));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      autoSendableChooser.addOption("Red Line Middle", new SequentialCommandGroup(
          new UpdatePositionFromVision(visionUtil, driveUtil),
          new HomeCarriageAndLinkage(carriageUtil, linkageUtil),
          new TrajectoryDrive(driveUtil, visionUtil, getTrajectoryFromPath("Red Line Middle"))));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      autoSendableChooser.addOption("Red Line Right", new SequentialCommandGroup(
          new UpdatePositionFromVision(visionUtil, driveUtil),
          new HomeCarriageAndLinkage(carriageUtil, linkageUtil),
          new TrajectoryDrive(driveUtil, visionUtil, getTrajectoryFromPath("Red Line Right"))));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      autoSendableChooser.addOption("Red Amp Right", new SequentialCommandGroup(
          new UpdatePositionFromVision(visionUtil, driveUtil),
          new HomeCarriageAndLinkage(carriageUtil, linkageUtil),
          new ParallelCommandGroup(
              new TrajectoryDrive(driveUtil, visionUtil, getTrajectoryFromPath("Red Amp Right")),
              new MoveLinkageAndCarriage(linkageUtil, carriageUtil, Constants.Carriage.carriageAmp,
                  Constants.Linkage.linkageAmp)),
          new AutoDumpAmp(carriageUtil)));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      autoSendableChooser.addOption("Red Amp Middle", new SequentialCommandGroup(
          new UpdatePositionFromVision(visionUtil, driveUtil),
          new HomeCarriageAndLinkage(carriageUtil, linkageUtil),
          new ParallelCommandGroup(
              new TrajectoryDrive(driveUtil, visionUtil, getTrajectoryFromPath("Red Amp Middle")),
              new MoveLinkageAndCarriage(linkageUtil, carriageUtil, Constants.Carriage.carriageAmp,
                  Constants.Linkage.linkageAmp)),
          new AutoDumpAmp(carriageUtil)));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      autoSendableChooser.addOption("Red Amp Left", new SequentialCommandGroup(
          new UpdatePositionFromVision(visionUtil, driveUtil),
          new HomeCarriageAndLinkage(carriageUtil, linkageUtil),
          new ParallelCommandGroup(
              new TrajectoryDrive(driveUtil, visionUtil, getTrajectoryFromPath("Red Amp Left")),
              new MoveLinkageAndCarriage(linkageUtil, carriageUtil, Constants.Carriage.carriageAmp,
                  Constants.Linkage.linkageAmp)),
          new AutoDumpAmp(carriageUtil)));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      autoSendableChooser.addOption("Blue Amp Right", new SequentialCommandGroup(
          new UpdatePositionFromVision(visionUtil, driveUtil),
          new HomeCarriageAndLinkage(carriageUtil, linkageUtil),
          new ParallelCommandGroup(
              new TrajectoryDrive(driveUtil, visionUtil, getTrajectoryFromPath("Blue Amp Right")),
              new MoveLinkageAndCarriage(linkageUtil, carriageUtil, Constants.Carriage.carriageAmp,
                  Constants.Linkage.linkageAmp)),
          new AutoDumpAmp(carriageUtil)));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      autoSendableChooser.addOption("Blue Amp Middle", new SequentialCommandGroup(
          new UpdatePositionFromVision(visionUtil, driveUtil),
          new HomeCarriageAndLinkage(carriageUtil, linkageUtil),
          new ParallelCommandGroup(
              new TrajectoryDrive(driveUtil, visionUtil, getTrajectoryFromPath("Blue Amp Middle")),
              new MoveLinkageAndCarriage(linkageUtil, carriageUtil, Constants.Carriage.carriageAmp,
                  Constants.Linkage.linkageAmp)),
          new AutoDumpAmp(carriageUtil)));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      autoSendableChooser.addOption("Blue Amp Left", new SequentialCommandGroup(
          new UpdatePositionFromVision(visionUtil, driveUtil),
          new HomeCarriageAndLinkage(carriageUtil, linkageUtil),
          new ParallelCommandGroup(
              new TrajectoryDrive(driveUtil, visionUtil, getTrajectoryFromPath("Blue Amp Left")),
              new MoveLinkageAndCarriage(linkageUtil, carriageUtil, Constants.Carriage.carriageAmp,
                  Constants.Linkage.linkageAmp)),
          new AutoDumpAmp(carriageUtil)));
    } catch (Exception e) {
      e.printStackTrace();
    }

    SmartDashboard.putData("Auto Mode", autoSendableChooser);
  }

  /**
   * Method used to configure the default commands for all subsystems
   */
  private void configureDefaultCommands() {
    // DriveUtil - default command drives the robot
    driveUtil.setDefaultCommand(new TeleopSwerve(driveUtil));
    climbUtil.setDefaultCommand(new OperateClimb(climbUtil, Constants.Climb.motorStopSpeed));
    carriageUtil.setDefaultCommand(new OperateCarriage(carriageUtil));
    linkageUtil.setDefaultCommand(new OperateLinkage(linkageUtil));
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An example command will be run in autonomous
    return autoSendableChooser.getSelected();
  }

  public static XboxController getDriver() {
    return driver.getHID();
  }

  public static PS4Controller getOperator() {
    return operator.getHID();
  }

  public static boolean isSlow() {
    return isSlow;
  }

  /**
   *
   * 
   * @param trajectoryFile the input path relative to the deploy folder.
   */
  private File getTrajectoryFromPath(String trajectoryFile) throws Exception {
    File deploy;

    deploy = Filesystem.getDeployDirectory().toPath().resolve("paths/" + trajectoryFile + ".wpilib.json").toFile();
    return deploy;
  }
}
