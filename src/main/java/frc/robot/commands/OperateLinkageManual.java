// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.RobotContainer;
import frc.robot.subsystems.LinkageUtil;

public class OperateLinkageManual extends Command {
  LinkageUtil linkageUtil;
  int setpoint;
  boolean hasSetSetpoint;

  /** Creates a new OperateLinkageDefault. */
  public OperateLinkageManual(LinkageUtil linkageUtil) {
    this.linkageUtil = linkageUtil;
    hasSetSetpoint = false;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(linkageUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double joystickValue = RobotContainer.getOperator().getRightY();
    if (joystickValue < -0.1) {
      linkageUtil.setLinkageSetpoint(linkageUtil.getLinkagePositionTicks() + joystickValue * 12);
      hasSetSetpoint = false;
    } else if (joystickValue > 0.1) {
      linkageUtil.setLinkageSetpoint(linkageUtil.getLinkagePositionTicks() + joystickValue * 12);
      hasSetSetpoint = false;
    } else if (!hasSetSetpoint) {
      linkageUtil.setLinkageSetpoint(linkageUtil.getLinkagePositionTicks());
      hasSetSetpoint = true;
    }
    
    if (linkageUtil.isLimitPressed() && joystickValue < -0.1) {
      linkageUtil.resetEncoder();
      linkageUtil.overrideMotorSpeed(0);
    } else {
      linkageUtil.calculatePIDController();
    }


  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
