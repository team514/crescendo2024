// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import java.io.File;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryUtil;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.VisionUtil;

public class TrajectoryDrive extends Command {
  DriveUtil driveUtil;
  VisionUtil visionUtil;
  Trajectory trajectory;
  long startTime;
  boolean started, done;

  /** Creates a new TrajectoryDrive. */
  public TrajectoryDrive(DriveUtil driveUtil, VisionUtil visionUtil, File file) throws Exception {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveUtil);
    this.driveUtil = driveUtil;
    this.visionUtil = visionUtil;

    trajectory = TrajectoryUtil.fromPathweaverJson(file.toPath());
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    startTime = System.currentTimeMillis();
    started = false;
    done = false;

    if (!visionUtil.hasTarget()) {
      driveUtil.resetOdometry(trajectory.sample(0).poseMeters);
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    System.out.println("Running B");
    if (!started) {
      // Run this on first execute call
      System.out.println("Starting trajectory");
      started = true;
    }
    if (getLapseTime() / 1000.0 >= trajectory.getTotalTimeSeconds()) {
      // If the trajectory is done, stop motors
      done = true;
      driveUtil.driveRobot(new Translation2d(), 0, false, false);
    } else {
      // If the trajectory isn't done, continue running it
      Trajectory.State goal = trajectory.sample(getLapseTime() / 1000.0);
      driveUtil.driveTrajectory(goal);
      System.out.println("Running trajectory");
    }
  }

  public long getLapseTime() {
    return System.currentTimeMillis() - startTime;
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
