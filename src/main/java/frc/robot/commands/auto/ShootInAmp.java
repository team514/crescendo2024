// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.subsystems.CarriageUtil;

public class ShootInAmp extends Command {
  /** Creates a new ShootInAmp. */
  CarriageUtil carriageUtil;
  boolean done;

  public ShootInAmp(CarriageUtil carriageUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(carriageUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    done = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (carriageUtil.isNoteInCarriage()) {
      carriageUtil.setIOSpeed(Constants.Carriage.ioLeftSpeed, Constants.Carriage.ioRightSpeed);
    } else {
      carriageUtil.setIOSpeed(0, 0);
      done = true;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
