// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.CarriageUtil;
import frc.robot.subsystems.LinkageUtil;

public class ZeroLinkageAndCradle extends Command {
  /** Creates a new ZeroLinkageAndCradle. */
  CarriageUtil carriageUtil;
  LinkageUtil linkageUtil;

  public ZeroLinkageAndCradle(CarriageUtil carriageUtil, LinkageUtil linkageUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.carriageUtil = carriageUtil;
    this.linkageUtil = linkageUtil;

    addRequirements(carriageUtil, linkageUtil);

  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // if (!carriageUtil.isLimitPressed()) {
    //   carriageUtil.overrideMotorSpeed(0.25);
    // } else {
    //   carriageUtil.overrideMotorSpeed(0);
    //   carriageUtil.resetEncoder();
    //   carriageUtil.setCarriageSetpoint(0);
    // }

    // if (!linkageUtil.isLimitPressed()) {
    //   linkageUtil.overrideMotorSpeed(0.25);
    // } else {
    //   linkageUtil.overrideMotorSpeed(0);
    //   linkageUtil.resetEncoder();
    //   linkageUtil.setLinkageSetpoint(0);
    // }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    // linkageUtil.overrideMotorSpeed(0);
    // carriageUtil.overrideMotorSpeed(0);

    // linkageUtil.setLinkageSetpoint(linkageUtil.getLinkagePositionTicks());
    // carriageUtil.setCarriageSetpoint(carriageUtil.getCarriagePositionTicks());
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return (carriageUtil.isLimitPressed() && linkageUtil.isLimitPressed());
  }
}
