// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.subsystems.CarriageUtil;
import frc.robot.subsystems.LinkageUtil;

public class MoveLinkageAndCarriage extends Command {
  /** Creates a new MoveLinkageAndCarriage. */
  CarriageUtil carriageUtil;
  LinkageUtil linkageUtil;

  double linkageTarget, carriageTarget;

  final double carriageTargetDeadband = 1;

  boolean setLinkageSetpoint, setCarriageSetpoint, carriageDone;

  public MoveLinkageAndCarriage(LinkageUtil linkageUtil, CarriageUtil carriageUtil, double carriageTarget,
      double linkageTarget) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.linkageUtil = linkageUtil;
    this.carriageUtil = carriageUtil;
    this.linkageTarget = linkageTarget;
    this.carriageTarget = carriageTarget;

    addRequirements(carriageUtil, linkageUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    // if (carriageTarget == 0) {
    //   carriageTarget = -10;
    // }

    if (linkageTarget == 0) {
      linkageTarget = -10;
    }

    linkageUtil.setLinkageSetpoint(linkageTarget);
    setLinkageSetpoint = true;
    carriageDone = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    System.out.println("Running A " + carriageDone + "" + linkageUtil.isAtSetpoint() + "" + setLinkageSetpoint);
    linkageUtil.calculatePIDController();
    // carriageUtil.calculatePIDController();

    if (linkageUtil.isLimitPressed() && (linkageTarget < linkageUtil.getLinkagePositionTicks())) {
      linkageUtil.resetEncoder();
      linkageUtil.setLinkageSetpoint(0);
      setLinkageSetpoint = true;
    }

    if (carriageUtil.isLimitPressed()) {
      carriageUtil.resetEncoder();
      // carriageUtil.setCarriageSetpoint(0);
      // setCarriageSetpoint = true;
    }

    // if (!setCarriageSetpoint) {
    //   if (linkageUtil.getLinkagePositionTicks() > 65 * 42) {
    //     carriageUtil.setCarriageSetpoint(carriageTarget);
    //     setCarriageSetpoint = true;
    //   }
    // }
    
    if (linkageUtil.getLinkagePositionTicks() < (65 * 42) && linkageTarget > 65 * 42) {
      carriageUtil.setCarriageSpeed(0);
    } else if (!carriageDone) {
      if (carriageUtil.getCarriagePositionTicks() < carriageTarget) {
        carriageUtil.setCarriageSpeed(Constants.Carriage.carriageMotorSpeed * -1);
      } else if (carriageUtil.getCarriagePositionTicks() > carriageTarget) {
        carriageUtil.setCarriageSpeed(Constants.Carriage.carriageMotorSpeed);
      } else if (carriageUtil.getCarriagePositionTicks() > carriageTarget - carriageTargetDeadband
          && carriageUtil.getCarriagePositionTicks() < carriageTarget + carriageTargetDeadband) {
        carriageDone = true;
        carriageUtil.setCarriageSpeed(0);
      }
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    linkageUtil.setLinkageSetpoint(linkageUtil.getLinkagePositionTicks());
    carriageUtil.setCarriageSetpoint(carriageUtil.getCarriagePositionTicks());
    // carriageUtil.setCarriageSpeed(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return carriageDone && linkageUtil.isAtSetpoint() && setLinkageSetpoint;
  }
}
