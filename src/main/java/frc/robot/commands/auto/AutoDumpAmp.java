// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.CarriageUtil;

public class AutoDumpAmp extends Command {
  /** Creates a new AutoDumpAmp. */

  CarriageUtil carriageUtil;
  long startTime;

  boolean done;

  public AutoDumpAmp(CarriageUtil carriageUtil) {
    this.carriageUtil = carriageUtil;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(carriageUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    startTime = System.currentTimeMillis();


  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    System.out.println("Running C");
    System.out.println("A");
    if (getLapseTime() < 3000) {
      System.out.println("B");
      carriageUtil.setIOSpeed(-1, -.75);
    } else {
    System.out.println("C");
      carriageUtil.setIOSpeed(0, 0);
      done = true;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }

  public long getLapseTime() {
    return System.currentTimeMillis() - startTime;
  }
}
