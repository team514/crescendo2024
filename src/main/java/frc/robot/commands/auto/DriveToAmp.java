// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import java.io.File;
import java.io.IOException;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryUtil;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.DriveUtil;

public class DriveToAmp extends Command {
  DriveUtil driveUtil;
  Trajectory trajectory;
  long startTime;
  boolean started, done;

  /** Creates a new DriveToAmp. */
  public DriveToAmp(DriveUtil driveUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveUtil);
    this.driveUtil = driveUtil;

    try {
      trajectory = TrajectoryUtil.fromPathweaverJson(Filesystem.getDeployDirectory().toPath().resolve("paths/DriveToAmp.wpilib.json"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    startTime = System.currentTimeMillis();
    started = false;
    done = false;
    driveUtil.resetOdometry(trajectory.sample(0).poseMeters);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (!started) {
      // Run this on first execute call
      started = true;
    }
    if (getLapseTime() / 1000.0 >= trajectory.getTotalTimeSeconds()) {
      // If the trajectory is done, stop motors
      done = true;
      driveUtil.driveRobot(new Translation2d(), 0, false, false);
    } else {
      // If the trajectory isn't done, continue running it
      Trajectory.State goal = trajectory.sample(getLapseTime() / 1000.0);
      driveUtil.driveTrajectory(goal);
    }
  }

  public long getLapseTime() {
    return System.currentTimeMillis() - startTime;
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
