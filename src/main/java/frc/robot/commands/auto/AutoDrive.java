// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.DriveUtil;

public class AutoDrive extends Command {
  double distance;
  DriveUtil driveUtil;
  Translation2d translation;
  boolean done = false;

  /** Creates a new AutoDrive. */
  public AutoDrive(double distance, DriveUtil driveUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveUtil);
    this.distance = distance;
    this.driveUtil = driveUtil;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    translation = new Translation2d(distance, new Rotation2d());
    driveUtil.zeroGyro();
    driveUtil.resetOdometry(new Pose2d());
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    driveUtil.driveRobot(translation, 0, false, false);
    if (driveUtil.getPose().getX() >= Units.feetToMeters(distance)) {
      done = true;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    driveUtil.driveRobot(new Translation2d(), 0, false, false);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
