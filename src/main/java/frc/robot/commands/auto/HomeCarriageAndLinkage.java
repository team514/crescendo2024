// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;


import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.CarriageUtil;
import frc.robot.subsystems.LinkageUtil;

public class HomeCarriageAndLinkage extends Command {
  /** Creates a new HomeCarriageAndLinkage. */
  CarriageUtil carriageUtil;
  LinkageUtil linkageUtil;
  boolean carriageDone = false;
  boolean linkageDone = false;

  public HomeCarriageAndLinkage(CarriageUtil carriageUtil, LinkageUtil linkageUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.carriageUtil = carriageUtil;
    this.linkageUtil = linkageUtil;
    addRequirements(carriageUtil, linkageUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    carriageDone = false;
    linkageDone = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (!carriageDone) {
      if (!carriageUtil.isLimitPressed()) {
        carriageUtil.setCarriageSpeed(0.2);
      } else {
        carriageDone = true;
        carriageUtil.setCarriageSpeed(0);
      }
    }

    if (!linkageDone) {
      if (!linkageUtil.isLimitPressed()) {
        linkageUtil.overrideMotorSpeed(-0.2);
      } else {
        linkageDone = true;
        linkageUtil.overrideMotorSpeed(0);
      }
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return carriageDone && linkageDone;
  }
}
