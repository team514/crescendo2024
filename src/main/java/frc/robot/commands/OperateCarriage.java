// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.subsystems.CarriageUtil;

public class OperateCarriage extends Command {
  /** Creates a new OperateCarriage. */

  CarriageUtil carriageUtil;

  public OperateCarriage(CarriageUtil carriageUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.carriageUtil = carriageUtil;

    addRequirements(carriageUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double rightJoystickY = RobotContainer.getOperator().getRightY();
    rightJoystickY =  MathUtil.applyDeadband(rightJoystickY, 0.1);
    if (carriageUtil.isLimitPressed() && rightJoystickY > 0) {
      carriageUtil.resetEncoder();
      carriageUtil.setCarriageSpeed(0);
    } else if (carriageUtil.getCarriagePositionTicks() >= Constants.Carriage.maxCarriagePosition && rightJoystickY < 0) {
      carriageUtil.setCarriageSpeed(0);
    } else {
      carriageUtil.setCarriageSpeed(rightJoystickY * 0.35);
    }

    // carriageUtil.calculatePIDController();
    // if (speed == 0) {
    //   carriageUtil.overrideMotorSpeed(speed);
    // } else if (carriageUtil.isLimitPressed() && speed < 0) {
    //   carriageUtil.overrideMotorSpeed(speed);
    // } else if (carriageUtil.getCarriagePositionTicks() >= Constants.Carriage.maxCarriagePosition && speed < 0) {
    //   carriageUtil.overrideMotorSpeed(0);
    // } else if (!carriageUtil.isLimitPressed()) {
    //   carriageUtil.overrideMotorSpeed(speed);
    // }

    carriageUtil.setIOSpeed(0, 0);

    // if (carriageUtil.isLimitPressed()) {
    //   carriageUtil.resetEncoder();
    // }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
