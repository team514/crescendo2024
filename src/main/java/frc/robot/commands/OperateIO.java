// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.CarriageUtil;

public class OperateIO extends Command {
  /** Creates a new OperateIO. */
  CarriageUtil carriageUtil;
  double leftSpeed, rightSpeed;
  public OperateIO(CarriageUtil carriageUtil, double leftSpeed, double rightSpeed) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.carriageUtil = carriageUtil;
    this.leftSpeed = leftSpeed;
    this.rightSpeed = rightSpeed;
    addRequirements(carriageUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    carriageUtil.setIOSpeed(leftSpeed, rightSpeed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    carriageUtil.setIOSpeed(0, 0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
