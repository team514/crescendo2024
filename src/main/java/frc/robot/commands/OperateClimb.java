// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.subsystems.ClimbUtil;

public class OperateClimb extends Command {
  ClimbUtil climbUtil;
  double speed;

  /** Creates a new OperateClimb. */
  public OperateClimb(ClimbUtil climbUtil, double speed) {
    this.climbUtil = climbUtil;
    this.speed = speed;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(climbUtil);

  }

  // poopfart
  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (climbUtil.isLimitPressed() || climbUtil.getGrappleState()) {
      climbUtil.setMotorSpeed(0);
    } else {
      climbUtil.setMotorSpeed(speed);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
