// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.subsystems.DriveUtil;

public class TeleopSwerve extends Command {
  private DriveUtil driveUtil;

  private SlewRateLimiter translationLimiter = new SlewRateLimiter(3.0); // 3 meters per second^2
  private SlewRateLimiter strafeLimiter = new SlewRateLimiter(3.0);
  private SlewRateLimiter rotationLimiter = new SlewRateLimiter(3.0);

  private double translationValue = 0;
  private double strafeValue = 0;
  private double rotationValue = 0;
  private Translation2d translation2d = null;

  /** Creates a new TeleopSwerve. */
  public TeleopSwerve(DriveUtil driveUtil) {
    this.driveUtil = driveUtil;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveUtil);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    /* Get raw values from joysticks */
    double joystickY = RobotContainer.getDriver().getLeftY();
    double joystickX = RobotContainer.getDriver().getLeftX();
    double joystickZ = RobotContainer.getDriver().getRightX();

    if (RobotContainer.isSlow()) {
      joystickY *= 0.66;
      joystickX *= 0.66;
      joystickZ *= 0.66;
    }

    // This absolute value devision is just to make sure the absolute value is not always positive.
    translationValue = (Math.pow(joystickY, 3)) * -1; //* (joystickY / Math.abs(joystickY));
    strafeValue = (Math.pow(joystickX, 3)) * -1; //* (joystickX / Math.abs(joystickX));
    rotationValue = (Math.pow(joystickZ, 3)) * -1; // * (joystickZ / Math.abs(joystickZ));

    // boolean translationNegative = translationValue < 0;
    // boolean strafeNegative = strafeValue < 0;
    // // boolean rotationNegative = rotationValue < 0;

    // translationValue = Math.pow(translationValue,2);
    // strafeValue = Math.pow(strafeValue,2);
    // rotationValue = Math.pow(rotationValue,3);

    // if (translationNegative) {
    //   translationValue *= -1;
    // }

    // if (strafeNegative) {
    //   strafeValue *= -1;
    // }

    /* Apply deadband to avoid joystick drift */
    translationValue = MathUtil.applyDeadband(translationValue, Constants.Swerve.stickDeadband);
    strafeValue = MathUtil.applyDeadband(strafeValue, Constants.Swerve.stickDeadband);
    rotationValue = MathUtil.applyDeadband(rotationValue, Constants.Swerve.stickDeadband);

    /* Apply slew rate limiter, keep velocity under 3 meters per second */
    translationValue = translationLimiter.calculate(translationValue);
    strafeValue = strafeLimiter.calculate(strafeValue);
    rotationValue = rotationLimiter.calculate(rotationValue);

    /* Scale value up to velocity */ 
    /* Meters per second for translation/strafe */
    translationValue *= Constants.Swerve.maxSpeed;
    strafeValue *= Constants.Swerve.maxSpeed;
    /* Radians per second for rotation */
    rotationValue *= Constants.Swerve.maxAngularVelocity;

    translation2d = new Translation2d(translationValue, strafeValue);

    driveUtil.driveRobot(translation2d, rotationValue, driveUtil.isFieldRelative(), false);
  }
}
