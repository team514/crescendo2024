// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.subsystems.LinkageUtil;

public class OperateLinkage extends Command {
  LinkageUtil linkageUtil;
  int setpoint;
  boolean hasSetSetpoint;

  /** Creates a new OperateLinkageDefault. */
  public OperateLinkage(LinkageUtil linkageUtil) {
    this.linkageUtil = linkageUtil;
    hasSetSetpoint = false;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(linkageUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double leftJoystickY = RobotContainer.getOperator().getLeftY();
    leftJoystickY =  MathUtil.applyDeadband(leftJoystickY, 0.1);
    if (linkageUtil.isLimitPressed() && leftJoystickY < 0) {
      linkageUtil.resetEncoder();
      linkageUtil.setLinkageSetpoint(0);
    } else if (linkageUtil.getLinkagePositionTicks() >= Constants.Linkage.maxLinkagePosition && leftJoystickY < 0) {
      linkageUtil.setLinkageSetpoint(linkageUtil.getLinkagePositionTicks());
    } else if (leftJoystickY != 0) {
      linkageUtil.setLinkageSetpoint(linkageUtil.getLinkagePositionTicks() + (leftJoystickY * 20 * 42));
    }
    linkageUtil.calculatePIDController();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
