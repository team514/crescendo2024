// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.RobotContainer;
import frc.robot.subsystems.CarriageUtil;

public class OperateCarriageManual extends Command {
  /** Creates a new OperateCarriageManual. */
  CarriageUtil carriageUtil;
  private double joystickDeadband = 0.1;
  public OperateCarriageManual(CarriageUtil carriageUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.carriageUtil = carriageUtil;
    addRequirements(carriageUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double joystickValue = RobotContainer.getOperator().getLeftY();

    // if (joystickValue > joystickDeadband && joystickValue < -joystickDeadband) {
    //   carriageUtil.setCarriageSpeed(joystickValue); //.5 is to prevent going to fast. constantize?
    // } else {
    //   carriageUtil.setCarriageSpeed(0.0);
    // }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
