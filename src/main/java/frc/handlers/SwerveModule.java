package frc.handlers;

import com.ctre.phoenix6.StatusSignal;
import com.ctre.phoenix6.configs.CANcoderConfiguration;
import com.ctre.phoenix6.configs.MagnetSensorConfigs;
import com.ctre.phoenix6.hardware.CANcoder;
import com.ctre.phoenix6.signals.AbsoluteSensorRangeValue;
import com.ctre.phoenix6.signals.SensorDirectionValue;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.SparkPIDController;
import com.revrobotics.CANSparkBase.ControlType;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.lib.config.SwerveModuleConstants;
import frc.lib.util.CANCoderUtil;
import frc.lib.util.CANSparkMaxUtil;
import frc.lib.util.SwerveMathUtil;
import frc.lib.util.CANCoderUtil.CCUsage;
import frc.lib.util.CANSparkMaxUtil.Usage;
import frc.robot.Constants;

public class SwerveModule {
  public int moduleNumber;
  public Rotation2d lastAngle;
  private final Rotation2d angleOffset;

  private CANSparkMax angleMotor;
  private CANSparkMax driveMotor;

  private RelativeEncoder driveEncoder;
  private RelativeEncoder integratedAngleEncoder;
  private CANcoder angleEncoder;

  private final SparkPIDController driveController;
  private final SparkPIDController angleController;

  private final SimpleMotorFeedforward feedforward =
      new SimpleMotorFeedforward(
          Constants.Swerve.driveKS, Constants.Swerve.driveKV, Constants.Swerve.driveKA);
  
  public SwerveModule(int moduleNumber, SwerveModuleConstants moduleConstants) {
    this.moduleNumber = moduleNumber;
    angleOffset = moduleConstants.angleOffset;

    /* Angle Encoder Config */
    angleEncoder = new CANcoder(moduleConstants.cancoderID);
    configureAngleEncoder();

    /* Angle Motor Config */
    angleMotor = new CANSparkMax(moduleConstants.angleMotorID, MotorType.kBrushless);
    integratedAngleEncoder = angleMotor.getEncoder();
    angleController = angleMotor.getPIDController();
    configureAngleMotor();

    /* Drive Motor Config */
    driveMotor = new CANSparkMax(moduleConstants.driveMotorID, MotorType.kBrushless);
    driveEncoder = driveMotor.getEncoder();
    driveController = driveMotor.getPIDController();
    configureDriveMotor();

    lastAngle = getState().angle;
  }

  private void configureAngleEncoder() {
    CANCoderUtil.setCANCoderBusUsage(angleEncoder, CCUsage.kMinimal);

    CANcoderConfiguration swerveCanCoderConfig = new CANcoderConfiguration();
    MagnetSensorConfigs magnetSensorConfig = swerveCanCoderConfig.MagnetSensor;

    /* Swerve CANCoder Configuration */
    magnetSensorConfig.AbsoluteSensorRange = AbsoluteSensorRangeValue.Unsigned_0To1;
    magnetSensorConfig.SensorDirection = SensorDirectionValue.CounterClockwise_Positive;

    angleEncoder.setPosition(getAbsoluteAngle().getRotations());
    angleEncoder.getConfigurator().apply(swerveCanCoderConfig);
  }

  private void configureAngleMotor() {
    // Reset motor to factory defaults
    angleMotor.restoreFactoryDefaults();
    
    // Limit Spark Max CAN bus usage for non-positional data
    // Too much noise on the CAN bus with the number of CAN IDs swerve has causes issues
    CANSparkMaxUtil.setCANSparkMaxBusUsage(angleMotor, Usage.kPositionOnly);

    // Limit Spark Max current draw to protect from hardware failure
    angleMotor.setSmartCurrentLimit(Constants.Swerve.angleContinuousCurrentLimit);

    // Set whether the motor should be logically inverted
    angleMotor.setInverted(Constants.Swerve.angleInvert);

    // Set the neutral mode (brake or coast)
    angleMotor.setIdleMode(Constants.Swerve.angleNeutralMode);

    // Set the conversion factor for the integrated Spark Max encoder
    // This converts the encoder output into the angle of the wheel
    // 360 / gearRatio (see angleConversionFactor)
    integratedAngleEncoder.setPositionConversionFactor(Constants.Swerve.angleConversionFactor);

    // Set P/I/D/FF constants
    angleController.setP(Constants.Swerve.angleKP);
    angleController.setI(Constants.Swerve.angleKI);
    angleController.setD(Constants.Swerve.angleKD);
    angleController.setFF(Constants.Swerve.angleKFF);

    // Enables voltage compenstation and sets the target voltage to 12V
    // Robot batteries will drop in voltage throughout a match
    // This instructs the Spark Max to compenstate for that and maintain a constant 12V to the motor
    angleMotor.enableVoltageCompensation(Constants.Swerve.voltageComp);

    // Save this configuration to flash memory on the Spark Max
    angleMotor.burnFlash();

    // Set the integratedAngleEncoder current position to the absolute angle from the CANcoder
    resetToAbsolute();
  }

  private void configureDriveMotor() {
    // Reset motor to factory defaults
    driveMotor.restoreFactoryDefaults();
    
    // Allow full Spark Max CAN bus usage for all data
    // Too much noise on the CAN bus with the number of CAN IDs swerve has causes issues
    // However, the drive motors need to communicate all data for proper operation
    CANSparkMaxUtil.setCANSparkMaxBusUsage(driveMotor, Usage.kAll);

    // Limit Spark Max current draw to protect from hardware failure
    driveMotor.setSmartCurrentLimit(Constants.Swerve.driveContinuousCurrentLimit);

    // Set whether the motor should be logically inverted
    driveMotor.setInverted(Constants.Swerve.driveInvert);

    // Set the neutral mode (brake or coast)
    driveMotor.setIdleMode(Constants.Swerve.driveNeutralMode);

    // Set the velocity conversion factor for the integrated Spark Max encoder
    // This converts the encoder output into the meters per second the robot will move per RPM of the drive motor
    driveEncoder.setVelocityConversionFactor(Constants.Swerve.driveConversionVelocityFactor);
    // This converts the encoder output into the distance in meters the robot will move per RPM of the drive motor
    driveEncoder.setPositionConversionFactor(Constants.Swerve.driveConversionPositionFactor);

    // Set P/I/D/FF constants
    driveController.setP(Constants.Swerve.angleKP);
    driveController.setI(Constants.Swerve.angleKI);
    driveController.setD(Constants.Swerve.angleKD);
    driveController.setFF(Constants.Swerve.angleKFF);

    // Enables voltage compenstation and sets the target voltage to 12V
    // Robot batteries will drop in voltage throughout a match
    // This instructs the Spark Max to compenstate for that and maintain a constant 12V to the motor
    driveMotor.enableVoltageCompensation(Constants.Swerve.voltageComp);

    // Save this configuration to flash memory on the Spark Max
    driveMotor.burnFlash();

    // Reset driveEncoder to 0 ticks (aka 0 meters driven)
    driveEncoder.setPosition(0.0);
  }

  /**
   * Set the current position of the angle motor's integrated encoder
   * to match the reading of the CANcoder's absolute position value.
   * 
   * This allows us to perform PID calculations on the Spark Max
   * with the motor's integrated encoder instead of on the roboRIO
   * using the values from the CANcoder.
   */
  public void resetToAbsolute() {
    double absolutePosition = getAbsoluteAngle().getDegrees() - angleOffset.getDegrees();
    // double absolutePosition = getAbsoluteAngle().getDegrees();

    if (absolutePosition < 0 && integratedAngleEncoder.getPosition() > 0) {
      absolutePosition = absolutePosition + 360;
    } else if (absolutePosition > 0 && integratedAngleEncoder.getPosition() < 0) {
      absolutePosition = absolutePosition - 360;
    }

    integratedAngleEncoder.setPosition(absolutePosition);
  }

  public void moveWheelToHome() {
    // Ensure the angle motor's integrated encoder is equal to the absolute angle of the CANcoder
    // resetToAbsolute();

    // 0 degrees is "straight" since the encoder is offset by the angleOffset value
    SwerveModuleState test = new SwerveModuleState(0, new Rotation2d());
    setDesiredState(test, false);
    angleController.setReference(0, ControlType.kPosition);
    lastAngle = new Rotation2d();
  }

  /**
   * Get the angle of the drive wheel based on the value of the
   * Spark Max RelativeEncoder for the angle drive motor.
   * 
   * This method does not use the absolute CANcoder!
   * 
   * This method returns a Rotation2d object. You can get the angle
   * in various units with the following method calls:
   * - Rotation2d.getDegrees();
   * - Rotation2d.getRadians();
   * - Rotation2d.getRotations();
   */
  public Rotation2d getAngle() {
    return Rotation2d.fromDegrees(integratedAngleEncoder.getPosition());
  }

  /**
   * Get the angle of the drive wheel based
   * on the value of the CTRE CANcoder.
   * 
   * This method does not use the absolute CANcoder!
   * 
   * This method returns a Rotation2d object. You can get the angle
   * in various units with the following method calls:
   * - Rotation2d.getDegrees();
   * - Rotation2d.getRadians();
   * - Rotation2d.getRotations();
   */
  public Rotation2d getAbsoluteAngle() {
    StatusSignal<Double> signal = angleEncoder.getAbsolutePosition();
    return Rotation2d.fromRotations(signal.getValue());
  }

  public SwerveModuleState getState() {
    return new SwerveModuleState(driveEncoder.getVelocity(), getAngle());
    
    // Might need to be something like this:
    // return new SwerveModuleState(
    //   Conversions.RPSToMPS(mDriveMotor.getVelocity().getValue(), Constants.Swerve.wheelCircumference), 
    //   Rotation2d.fromRotations(mAngleMotor.getPosition().getValue())
    // );
  }

  public SwerveModulePosition getPosition() {
    return new SwerveModulePosition(driveEncoder.getPosition(), getAngle());

    // Might need to be something like this:
    // return new SwerveModulePosition(
    //   Conversions.rotationsToMeters(mDriveMotor.getPosition().getValue(), Constants.Swerve.wheelCircumference), 
    //   Rotation2d.fromRotations(mAngleMotor.getPosition().getValue())
    // );
  }

  /**
   * Update the desired state of the swerve module.
   * 
   * @param desiredState The new desired state of the swerve module
   * @param isClosedLoop True if PID should manage movement of the drive motors, false if otherwise; PID always manages angle motors
   */
  public void setDesiredState(SwerveModuleState desiredState, boolean isClosedLoop) {
    // Minimize the change in heading the desired swerve module state would require by
    // potentially reversing the direction the wheel spins. For example, if the desiredState
    // would have the wheel rotate CW 170 degrees, just rotate CCW 10 degrees instead, and
    // drive the wheel in the opposite direction.
    //
    // WPILib has a built-in method that does this SwerveModuleState.optimize. However,
    // this method assumes "continuous input", which in our case means that it assumes
    // 400 degrees is a valid angle to pass to the PID controller as a target angle.
    // Unfortunately, CTRE and REV PID controllers don't support continuous input, so
    // we instead need to pass 40 degrees in that circumstance. This has implications
    // on the optimize math, which is why a custom method was written.
    desiredState = SwerveMathUtil.optimizeModuleAngles(desiredState, getAngle());

    // Set the desired angle on the angle motor's PID controller
    setTargetAngle(desiredState);

    // Set the desired velocity on the drive motor.
    // If isClosedLoop = true, then PID is used to manage the velocity of the drive motor.
    // If isClosedLoop = false (aka open-loop), we directly set the speed of the drive motor.
    setTargetSpeed(desiredState, isClosedLoop);
  }

  /**
   * Set the desired angle of the angle motor
   * 
   * @param desiredState The full desiredState object, which includes the desired angle
   */
  public void setTargetAngle(SwerveModuleState desiredState) {
    // Prevent rotating module if speed is less then 1%. Prevents jittering.
    Rotation2d angle;
    if (Math.abs(desiredState.speedMetersPerSecond) <= (Constants.Swerve.maxSpeed * 0.01)) {
      angle = lastAngle;
    } else {
      angle = desiredState.angle;
    }

    // Set the target position of the angle motor's PID controller.
    // Since this PID controller manages the angle of the wheel, the setpoint unit is degrees.
    angleController.setReference(angle.getDegrees(), ControlType.kPosition);
    lastAngle = angle;
  }

  /**
   * set the desired velocity of the drive motor
   * 
   * @param desiredState The full desiredState object, which includes the desired drive velocity
   * @param isClosedLoop True if closed-loop (PID control), false if open-loop (direct drive)
   */
  public void setTargetSpeed(SwerveModuleState desiredState, boolean isClosedLoop) {
    if (isClosedLoop) {
      // Set the target velocity of the drive motor's PID controller.
      // Calculate a feedforward value based on the target velocity.
      driveController.setReference(
          desiredState.speedMetersPerSecond,
          ControlType.kVelocity,
          0,
          feedforward.calculate(desiredState.speedMetersPerSecond));
    } else {
      // Convert target velocity to percent output (aka 4.5 m/s becomes 100% power)
      double percentOutput = desiredState.speedMetersPerSecond / Constants.Swerve.maxSpeed;
      
      // Directly set speed value for the motor, in the range of -1 to 1
      driveMotor.set(percentOutput);
    }
  }

  public CANSparkMax getDriveMotor() {
    return driveMotor;
  }
}
